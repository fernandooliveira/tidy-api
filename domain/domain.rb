# base domain class
class Domain
  def self.database
    @@database ||= SQLite3::Database.open('main.db')
    @@database.results_as_hash = true

    @@database
  end
end
