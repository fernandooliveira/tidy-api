# encoding: utf-8

# class that represents a user
class User < Domain
  attr_accessor :id, :first_name, :last_name, :email, :phone, :created_at,
              :updated_at, :password

  def initialize(fields = {})
    return unless fields
    @id = fields['id']
    @first_name = fields['first_name']
    @last_name = fields['last_name']
    @email = fields['email']
    @phone = fields['phone']
    @created_at = fields['created_at']
    @updated_at = fields['updated_at']
    @password = fields['password']
  end

  def self.list
    list = User.database.execute('select * from users order by id desc')
    list.map { |u| User.new(u) }
  end

  def self.find(id)
    sql = 'SELECT * FROM users WHERE id = ?'
    user_hash = User.database.execute(sql, id).first
    User.new(user_hash)
  end

  def self.create(fields)
    database = User.database
    user = User.new(fields)

    insert = <<-SQL
      INSERT INTO users values
        (NULL, ?, ?, ?, ?, DATETIME('now'), DATETIME('now'), ?)
    SQL

    database.execute(insert, user.first_name, user.last_name,
                     user.email, user.phone,
                     Digest::SHA256.hexdigest(user.password))

    User.find(database.last_insert_row_id)
  end

  def self.check(email, password)
    password = Digest::SHA256.hexdigest(password)
    sql = "SELECT * FROM users WHERE email = '#{email}' and password = '#{password}'"
    hash = User.database.execute(sql).first

    User.new(hash)
  end

  def valid?
    return false if @email.nil? || @password.nil?
    true
  end
end
