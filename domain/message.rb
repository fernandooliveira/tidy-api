# encoding: utf-8

# class that represents a message
class Message < Domain
  attr_reader :id, :user_id, :user, :message, :created_at, :updated_at

  def initialize(fields)
    @id = fields['id']
    @user_id = fields['user_id']
    @user = User.find(fields['user_id']) if fields['user_id']
    @message = fields['message']
    @created_at = fields['created_at']
    @updated_at = fields['updated_at']
  end

  def self.list_timeline(logged_user_id)
    following = Follow.following_by(logged_user_id)
    following_ids = following.map(&:followed_id).join(', ')

    sql_timeline = <<-SQL
      SELECT * FROM messages WHERE user_id IN (%s) ORDER BY CREATED_AT DESC
    SQL
    list = Message.database.execute(sql_timeline % following_ids)
    list.map { |f| Message.new(f) }
  end

  def self.find(id)
    sql = 'SELECT * FROM messages WHERE id = ?'
    message_hash = Message.database.execute(sql, id).first
    Message.new(message_hash)
  end

  def self.create(fields)
    database = Message.database
    message = Message.new(fields)

    insert = <<-SQL
      INSERT INTO messages values
        (NULL, ?, ?, DATETIME('now'), DATETIME('now'))
    SQL

    database.execute(insert, message.user_id, message.message)
    Message.find(database.last_insert_row_id)
  end

  def valid?
    return false if @message.nil?
    true
  end
end
