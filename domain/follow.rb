# encoding: utf-8

# class that represents a follow
class Follow < Domain
  attr_reader :id, :user_id, :user, :followed_id, :following, :created_at,
              :updated_at

  def initialize(fields)
    @id = fields['id']
    @user_id = fields['user_id']
    @user = User.find(fields['user_id']) if fields['user_id']
    @followed_id = fields['followed_id']
    @following = User.find(fields['followed_id']) if fields['followed_id']
    @created_at = fields['created_at']
    @updated_at = fields['updated_at']
  end

  def self.following_by(logged_user_id)
    sql_following = 'SELECT * FROM follows WHERE user_id = ?'
    following = Follow.database.execute(sql_following, logged_user_id)
    following.map { |f| Follow.new(f) }
  end

  def self.find(user_id, followed_id)
    sql = 'SELECT * FROM follows WHERE user_id = ? and followed_id = ?'
    data = Follow.database.execute(sql, user_id, followed_id).first
    Follow.new(data)
  end

  def self.create(fields)
    database = Follow.database
    follow = Follow.new(fields)

    insert = <<-SQL
      INSERT INTO follows values
        (?, ?, DATETIME('now'), DATETIME('now'))
    SQL

    database.execute(insert, follow.user_id, follow.followed_id)
    Follow.find(follow.user_id, follow.followed_id)
  end

  def valid?
    return false if @followed_id.nil? || @user_id.nil?
    true
  end
end
