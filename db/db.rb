# encoding: utf-8

module DB
  def self.setup(dbname)
    database = SQLite3::Database.new dbname

    create_users_table(database)
    create_messages_table(database)
    create_follows_database(database)
  end

  def self.create_users_table(database)
    database.execute(
      <<-SQL
      CREATE TABLE users (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      first_name VARCHAR(255) NOT NULL,
      last_name VARCHAR(255) NOT NULL,
      email VARCHAR(255) NOT NULL UNIQUE,
      phone VARCHAR(255) NOT NULL,
      created_at DATETIME NOT NULL,
      updated_at DATETIME NOT NULL,
      password VARCHAR(255) NOT NULL
      );
      SQL
    )
  end

  def self.create_messages_table(database)
    database.execute(
      <<-SQL
      CREATE TABLE messages (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      user_id INTEGER,
      message VARCHAR(140),
      created_at DATETIME NOT NULL,
      updated_at DATETIME NOT NULL,
      FOREIGN KEY(user_id) REFERENCES users(id)
      );
      SQL
    )
  end

  def self.create_follows_table(database)
    database.execute(
      <<-SQL
      CREATE TABLE follows (
      user_id INTEGER,
      followed_id INTEGER,
      created_at DATETIME NOT NULL,
      updated_at DATETIME NOT NULL,
      FOREIGN KEY(user_id) REFERENCES users(id),
      FOREIGN KEY(followed_id) REFERENCES users(id)
      );
      SQL
    )
  end
end
