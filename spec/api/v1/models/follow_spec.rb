require_relative '../../../../config/application.rb'
require 'spec_helper'

RSpec.describe Follow, type: :model do

  it 'is valid with valid attributes' do
    expect(Follow.new('followed_id' => 1, 'user_id' => 2)).to be_valid
  end

  it 'not valid without attributes' do
    expect(Follow.new({})).to_not be_valid
  end
end
