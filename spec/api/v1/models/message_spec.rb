require_relative '../../../../config/application.rb'
require 'spec_helper'

RSpec.describe Message, type: :model do

  it 'is valid with valid attributes' do
    expect(Message.new('message' => 'New Post')).to be_valid
  end

  it 'not valid without attributes' do
    expect(Message.new({})).to_not be_valid
  end
end
