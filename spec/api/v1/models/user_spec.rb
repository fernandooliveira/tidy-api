require_relative '../../../../config/application.rb'
require 'spec_helper'

RSpec.describe User, type: :model do
  before(:each) do
    @user_hash = {
      'first_name' => 'Tonho',
      'last_name' => 'da Lua',
      'email' => 'tonho.da@lua.com',
      'phone' => '(82) 23123-1231',
      'password' => '123456'
    }
  end

  it 'is valid with valid attributes' do
    expect(User.new(@user_hash)).to be_valid
  end

  it 'not valid without attributes' do
    expect(User.new({})).to_not be_valid
  end
end
