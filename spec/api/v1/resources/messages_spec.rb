require_relative '../../../../config/application.rb'
require 'spec_helper'

describe API::Resources::Messages, type: :api do
  before(:each) do
    token = 'bmFuZG9vbGl2ZWlyYS5hbEBnbWFpbC5jb206MTIzNDU2'
    @http_login = { 'HTTP_AUTHORIZATION' => "Basic #{token}" }
  end

  context 'GET /api/v1/messages/timeline/:user_id' do
    it 'returns 200' do
      get '/api/v1/messages/timeline/1', nil, @http_login
      expect(last_response.status).to eq(200)
      expect(last_response.body).to_not be_nil
    end

    it 'returns 401' do
      get '/api/v1/messages/timeline/1'
      expect(last_response.status).to eq(401)
    end
  end
end
