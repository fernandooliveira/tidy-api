require 'rubygems'

require 'rack/test'
require_relative '../config/application.rb'

RSpec.configure do |config|
  config.color = true
  config.formatter = :documentation
  config.expect_with :rspec
  config.include Rack::Test::Methods, api: true
end
