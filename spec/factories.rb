FactoryGirl.define do
  factory :message do
    message 'Post Message'
    user_id 1
    created_at '2017-02-01 12:23:23'
    updated_at '2017-02-01 12:23:23'
  end

  factory :user do
    first_name 'Joseph'
    last_name 'Alfred'
    email 'test@test.com'
    password '123456'
    phone '55 82 88888-2323'
    created_at '2017-02-01 12:23:23'
    updated_at '2017-02-01 12:23:23'
  end
end