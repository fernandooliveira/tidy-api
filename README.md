# Configuration

This project requires Ruby 2.3.3.

To configure this project run the following commands:

```
bundle install
```

This project already has a database with sampledata created. Only run this command if you want to override existing database.
```
rake db:create
```

# Running

```
bin/rackup
```

# Testing

```
bundle exec rspec
```

# API Documentation

A documentation about this API endpoints can be found at http://localhost:9292/api/v1/swagger_doc

# Examples

## Get Users

```
curl -H 'Content-Type: application/json' http://localhost:9292/api/v1/users | python -m json.tool
```

## Create User

```
curl -u 'nandooliveira.al@gmail.com:123456' -d '{"first_name": "Tonho", "last_name": "da Lua", "email": "tonho.da.lua31223@lua.com", "phone": "(82) 23123-1231", "password": "123456"}' \
-H 'Content-Type: application/json' \
-X POST http://localhost:9292/api/v1/users | python -m json.tool
```

## Post Message

```
curl -u 'nandooliveira.al@gmail.com:123456' -d '{"user_id": "1", "message": "New post sent..."}' \
-H 'Content-Type: application/json' \
-X POST http://localhost:9292/api/v1/messages | python -m json.tool
```


## Get Timeline

```
curl -u 'nandooliveira.al@gmail.com:123456' -H 'Content-Type: application/json' \
http://localhost:9292/api/v1/messages/timeline/1 | python -m json.tool
```

## Follow Someone

```
curl -u 'nandooliveira.al@gmail.com:123456' -H 'Content-Type: application/json' \
-H 'Content-Length: 0' \
-X POST http://localhost:9292/api/v1/users/1/follow/5 | python -m json.tool
```
