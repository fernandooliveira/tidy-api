# encoding: utf-8

module API
  class ErrorHandler < Grape::Middleware::Base
    def call!(env)
      @env = env
      begin
        @app.call(@env)
      rescue Exception => e
        throw :error, message: e.message || options[:default_message], status: 422
      end
    end
  end
end
