require_relative '../config/application.rb'

# root api module
module API
  # resources
  require_relative 'resources/users.rb'
  require_relative 'resources/messages.rb'

  # entities
  require_relative 'entities/user.rb'
  require_relative 'entities/message.rb'

  require_relative 'errors.rb'

  # Tidy Api v1
  class Tidy < Grape::API
    version 'v1', usage: :path
    prefix :api
    format :json

    http_basic do |email, password|
      user = User.check(email, password)
      user.email == email
    end

    use API::ErrorHandler

    # /api/v1/ping
    get '/ping' do
      { ping: 'pong' }
    end

    mount Resources::Users
    mount Resources::Messages

    add_swagger_documentation(
      api_version: 'v1',
      mount_path: 'doc',
      hide_documentation_path: true,
      markdown: true
    )
  end
end
