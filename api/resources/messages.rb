module API
  module Resources
    # resources for message model
    class Messages < Grape::API
      resource :messages do
        get 'timeline/:user_id' do
          list = Message.list_timeline(params[:user_id].to_i)
          present list, with: API::Entities::Message
        end

        post do
          # check for required params
          required_fields = %w(user_id message)
          unless (required_fields - params.keys).empty?
            raise ArgumentError 'Unprocessable Entity'
          end

          if params[:message].size > 140
            raise ArgumentError, 'Message should have maximum of 140 characters'
          end

          present Message.create(params), with: API::Entities::Message
        end
      end
    end
  end
end
