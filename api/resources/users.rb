module API
  module Resources
    # rest resources for user model
    class Users < Grape::API
      resource :users do
        get do
          users = User.list
          present users, with: API::Entities::User
        end

        post do
          # check for required params
          required_fields = %w(first_name last_name email phone)
          unless (required_fields - params.keys).empty?
            raise ArgumentError, 'Unprocessable Entity'
          end

          present User.create(params), with: API::Entities::User
        end

        post ':user_id/follow/:followed_id' do
          # check for required params
          required_fields = %w(user_id followed_id)
          unless (required_fields - params.keys).empty?
            raise ArgumentError, 'Unprocessable Entity'
          end

          present Follow.create(params), with: API::Entities::Follow
        end
      end
    end
  end
end
