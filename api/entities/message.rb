module API
  module Entities
    # entity for serialize message model
    class Message < Grape::Entity
      expose :id
      expose :user_id
      expose :user, using: API::Entities::User
      expose :message
      expose :created_at
      expose :updated_at
    end
  end
end
