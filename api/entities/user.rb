module API
  module Entities
    # entity for serialize user class
    class User < Grape::Entity
      expose :id
      expose :first_name
      expose :last_name
      expose :email
      expose :phone
      expose :created_at
      expose :updated_at
    end
  end
end
