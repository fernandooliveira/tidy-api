module API
  module Entities
    # entity for serialize follow model
    class Follow < Grape::Entity
      expose :id
      expose :user_id
      expose :user, using: API::Entities::User
      expose :followed_id
      expose :following, using: API::Entities::User
      expose :created_at
      expose :updated_at
    end
  end
end
